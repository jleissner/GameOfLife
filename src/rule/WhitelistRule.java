package rule;

/**
 * Created by Jan-Frederik Leißner on 20.06.2017.
 */
public class WhitelistRule extends Rule {

    private int from = 0;
    private int to = 0;
    private RuleOutcome outcome;

    public WhitelistRule(int from, int to, RuleOutcome outcome) {
        this.from = from;
        this.to = to;
        this.outcome = outcome;
    }

    public WhitelistRule(int value, RuleOutcome outcome){
        this(value,value,outcome);
    }

    @Override
    public boolean applies(int alive_neighbour_count) {
        return alive_neighbour_count>=from && alive_neighbour_count<=to;
    }

    @Override
    public boolean outcome(boolean current_status) {
        if(outcome==RuleOutcome.DIE){
            return false;
        }
        if(outcome==RuleOutcome.LIVE){
            return true;
        }
        return current_status;
    }
}
