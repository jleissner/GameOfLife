package rule;

/**
 * Created by Jan-Frederik Leißner on 20.06.2017.
 */
public class BlacklistRule extends WhitelistRule {
    public BlacklistRule(int from, int to, RuleOutcome outcome) {
        super(from, to, outcome);
    }

    public BlacklistRule(int value, RuleOutcome outcome) {
        super(value, outcome);
    }

    @Override
    public boolean applies(int alive_neighbour_count) {
        return !super.applies(alive_neighbour_count);
    }
}
