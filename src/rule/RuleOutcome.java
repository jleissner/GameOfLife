package rule;

/**
 * Created by Jan-Frederik Leißner on 20.06.2017.
 */
public enum RuleOutcome {
    LIVE, DIE, STAY;
}
