package rule;

import java.util.ArrayList;

/**
 * Created by Jan-Frederik Leißner on 20.06.2017.
 */
public class RuleList extends ArrayList<Rule>{
    public Rule getMatchingRule(int alive_neighbours) {
        for(Rule r : this){
            if(r.applies(alive_neighbours)){
                return r;
            }
        }
        return new WhitelistRule(0,0,RuleOutcome.STAY);
    }
}
