package rule;

/**
 * Created by Jan-Frederik Leißner on 20.06.2017.
 */
public abstract class Rule {
    public abstract boolean applies(int alive_neighbour_count);
    public abstract boolean outcome(boolean current_status);
}
