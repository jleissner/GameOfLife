import board.Board;
import board.Position;
import board.PositionList;
import rule.BlacklistRule;
import rule.RuleList;
import neighbourhood.MooreNeighbourhood;
import rule.RuleOutcome;
import rule.WhitelistRule;

public class Main {

    public static void main(String[] args) {
        PositionList cells = PositionList.parseString("X0X;0XXX;X0X",new Position(0,0),';','X','0');

        RuleList rules = new RuleList();
            rules.add(new WhitelistRule(3, RuleOutcome.LIVE));
            rules.add(new WhitelistRule(4, RuleOutcome.STAY));
            rules.add(new BlacklistRule(3,4,RuleOutcome.DIE));

        Board b = new Board(cells,rules,new MooreNeighbourhood(1));
    }
}
