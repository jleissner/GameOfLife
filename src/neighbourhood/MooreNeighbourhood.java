package neighbourhood;

import board.Board;
import board.Position;
import board.PositionList;
import javafx.geometry.Pos;

/**
 * Created by Jan-Frederik Leißner on 20.06.2017.
 */
public class MooreNeighbourhood extends Neighbourhood {
    private int range;

    public MooreNeighbourhood(int i) {
        super();
        range = i;
    }

    @Override
    public PositionList neighboursOf(Position p, Board b) {
        PositionList positions = new PositionList();
        for(int x= p.x-range;x <= p.x+range; x++){
            for(int y =p.y-range; y<=p.y+range; y++){
                positions.add(new Position(x,y));
            }
        }
        return positions;
    }
}
