package neighbourhood;

import board.Board;
import board.Position;
import board.PositionList;

/**
 * Created by Jan-Frederik Leißner on 20.06.2017.
 */
public class DefaultNeighbourhood extends Neighbourhood {
    public DefaultNeighbourhood() {
        super();
    }

    @Override
    public PositionList neighboursOf(Position p, Board b) {
        return new PositionList();
    }
}
