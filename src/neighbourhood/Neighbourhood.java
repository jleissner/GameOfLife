package neighbourhood;

import board.Board;
import board.Position;
import board.PositionList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jan-Frederik Leißner on 20.06.2017.
 */
public abstract class Neighbourhood {
    public int countAliveCellsAround(Position p, Board b){
        int counter = 0;
        for(Position current_position : neighboursOf(p,b)){
            if(b.getAliveCells().isAlive(current_position)){
                counter++;
            }
        }
        return counter;
    }
    public int countDeadCellsAround(Position p, Board b){
        int counter = 0;
        for(Position current_position : neighboursOf(p,b)){
            if(b.getAliveCells().isDead(current_position)){
                counter++;
            }
        }
        return counter;
    }
    public abstract PositionList neighboursOf(Position p, Board b);
}
