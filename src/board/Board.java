package board;

import neighbourhood.DefaultNeighbourhood;
import neighbourhood.Neighbourhood;
import rule.Rule;
import rule.RuleList;

/**
 * Created by Jan-Frederik Leißner on 20.06.2017.
 */
public class Board {
    private PositionList alive_cells;
    private RuleList rules;
    private Neighbourhood neighbourhood;

    public Board(PositionList alive_cells, RuleList rules,Neighbourhood neighbourhood) {
        this.alive_cells = alive_cells;
        this.rules = rules;
        this.neighbourhood = neighbourhood;
    }

    public Board(RuleList rules,Neighbourhood neighbourhood){
        this(new PositionList(),rules,neighbourhood);
    }

    public Board(){
        this(new RuleList(),new DefaultNeighbourhood());
    }

    public PositionList getAliveCells() {
        return alive_cells;
    }

    public void evolve(){
        PositionList evolveable = calculateEvolveablePositions();
        PositionList evolved = new PositionList();
        for(Position p : evolveable){
            evolveAtPosition(evolved, p);
        }
        alive_cells = evolved;
    }

    private void evolveAtPosition(PositionList evolved_list, Position p) {
        int alive_neighbours = neighbourhood.countAliveCellsAround(p,this);
        Rule matching_rule = rules.getMatchingRule(alive_neighbours);
        boolean current_status = alive_cells.isAlive(p);
        if(matching_rule.outcome(current_status)){
            evolved_list.add(p);
        }
    }

    private PositionList calculateEvolveablePositions() {
        PositionList evolveable = new PositionList();
        for(Position p : alive_cells){
            evolveable.addAll(neighbourhood.neighboursOf(p,this));
        }
        evolveable.removeDuplicates();
        return evolveable;
    }

    public void print() {
        int[] min_max = alive_cells.caluclateMinMax();
        String to_print = "Board-Ausschnitt:\n x-Achse: "+min_max[0]+" bis "+min_max[1]+"\n y-Achse: "+min_max[2]+" bis "+min_max[3]+"\n ";
        for(int col = min_max[0]; col <=min_max[1]; col++){
            to_print += "---";
        }
        to_print += "\n";
        for(int row = min_max[3]; row >= min_max[2]; row--){
            to_print += "|";
            for(int col = min_max[0]; col <=min_max[1]; col++){
                to_print += (alive_cells.isAlive(new Position(col,row))?" X ":"   ");
            }
            to_print += "|\n";
        }
        to_print += " ";
        for(int col = min_max[0]; col <=min_max[1]; col++){
            to_print += "---";
        }
        System.out.println(to_print);
    }
}
