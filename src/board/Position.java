package board;

/**
 * Created by Jan-Frederik Leißner on 20.06.2017.
 */
public class Position {
    public int x = 0;
    public int y = 0;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj.getClass().toString().equals(getClass().toString())){
            return obj.toString().equals(toString());
        }
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "("+x+"|"+y+")";
    }
}
