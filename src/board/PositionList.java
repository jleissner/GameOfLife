package board;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jan-Frederik Leißner on 20.06.2017.
 */
public class PositionList extends ArrayList<Position> {
    public boolean isAlive(Position p){
        return contains(p);
    }
    public boolean isDead(Position p){
        return !isAlive(p);
    }

    @Override
    public boolean add(Position position) {
        if(contains(position)){
            return false;
        }
        return super.add(position);
    }

    public void removeDuplicates(){
        Map<String,Position> single = new HashMap<String, Position>();
        for(Position p : this){
            single.put(p.toString(),p);
        }
        this.clear();
        this.addAll(single.values());
    }

    public void normalize(){
        int[] min_max = caluclateMinMax();
        for(Position p : this){
            p.x -= min_max[0];
            p.y -= min_max[2];
        }
    }

    public int[] caluclateMinMax(){
        int[] dimensions = new int[4];
        for(Position p : this){
            dimensions[0] = (p.x<dimensions[0]?p.x:dimensions[0]);
            dimensions[1] = (p.x>dimensions[1]?p.x:dimensions[1]);
            dimensions[2] = (p.y<dimensions[2]?p.y:dimensions[2]);
            dimensions[3] = (p.y>dimensions[3]?p.y:dimensions[3]);
        }
        return dimensions;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public static PositionList parseString(String s, Position top_left, char linebreak_symbol, char alive_symbol, char dead_symbol) {
        String[] rows = s.split(String.valueOf(linebreak_symbol));
        int counter_y = top_left.y;
        PositionList cells = new PositionList();
        for(String row : rows){
            char[] chars = row.toCharArray();
            int counter_x = top_left.x;
            for(char c : chars){
                if(c==alive_symbol && c!=dead_symbol){
                    cells.add(new Position(counter_x,counter_y));
                }
                counter_x++;
            }
            counter_y--;
        }
        cells.normalize();
        return cells;
    }

    public static PositionList parseRepresentation(String representation){
        Pattern p = Pattern.compile("(.)','(.)','(.)','(.*)");
        Matcher m = p.matcher(representation);
        if(m.find()){
            return parseString(m.group(4),new Position(0,0),m.group(1).charAt(0),m.group(2).charAt(0),m.group(3).charAt(0));
        }else{
            System.err.println("Representation nicht ladbar! ");
        }
        return new PositionList();
    }

    public String parseableRepresentation(){
        String save = "';','X','0','";
        int[] min_max = caluclateMinMax();
        for(int row = min_max[3]; row >= min_max[2]; row--){
            for(int col = min_max[0]; col <=min_max[1]; col++){
                save += (isAlive(new Position(col,row))?"X":"0");
            }
            save += ";";
        }
        save += "'";
        return save;
    }
}
